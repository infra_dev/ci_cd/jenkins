package com.infineon.pipelines

import com.infineon.stages.StageInterface

abstract class AbstractPipeline implements PipelineInterface
{
    protected def stages

    AbstractPipeline()
    {
        this.stages = []
    }

    def leftShift(StageInterface stage)
    // (StageInterface)
    {
        stages << stage
        stage
    }

    abstract void execute()
}
