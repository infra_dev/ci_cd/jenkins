package com.infineon.pipelines

import com.infineon.pipelines.AbstractPipeline
import com.infineon.stages.StageInterface

class Pipeline extends AbstractPipeline
{
    private def script
    private def timeout

    Pipeline(script, timeout=1440)
    {
        super()
        this.script = script
        this.timeout = timeout
    }

    void execute()
    {
        this.script.timeout(time: this.timeout)
        {
            for(StageInterface stage : this.stages)
            {
                stage.execute()
            }
        }
    }
}
