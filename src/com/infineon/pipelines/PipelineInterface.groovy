package com.infineon.pipelines

interface PipelineInterface extends Serializable
{
    void execute()
}
