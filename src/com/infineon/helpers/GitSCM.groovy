package com.infineon.helpers

class GitSCM implements Serializable
{
    def script
    def source
    def checkoutArgs
    def changeset_hash

    GitSCM(script, source)
    // (Object, String)
    {
        this.script = script
        this.source = source
        
        this.checkoutArgs = [
            $class: 'GitSCM',
            source: this.source,
            revision: 'main']
        
        this.changeset_hash = 'main'
    }

    def setCredentialsId(credentialsId)
    // (String)
    {
        this.checkoutArgs << [credentialsId: credentialsId]
        this
    }

    def setChangeset(changeset)
    // (String)
    {
        this.changeset_hash = changeset
        this
    }

    private def post_checkout()
    {
        this.script.sh 'git pull -f'
        this.script.sh "git update --rev ${this.changeset_hash}"
        this.script.sh 'git log > git_log.txt'
    }

    def checkout(Map args)
    {
        try {

            this.script.echo 'Try git clone with credentials'
            this.script withCredentials([gitUsernamePassword(credentialsId: 'fa4b8ad1-7d40-4f6c-b7b3-ec5c83db1c68', gitToolName: 'git')]) {

                this.script.sh "echo \"From withCredentials()\r\n\""

                /* Clone repo if directory doesn't exist, otherwise pull */
                
                this.script.sh "if cd ml-infra-dev-repo; then echo \'dir ml-infra-dev-repo exists\'; git checkout jp_dev; git pull --verbose; git log --graph > ../git_graph.log; else echo \'Cloning repo\'; git clone https://gitlab.intra.infineon.com/adsm/ml-infra-dev-repo.git; fi"
                this.script.sh "sleep 1"
                }
        }
        catch (e) {
            this.script.echo 'Repo update failed'
            println("Exception: ${e}")
        }
        /* TODO
        try
        {
            this.script.checkout(this.checkoutArgs << args)
            post_checkout()
        }
        catch (e)
        {
            this.script.echo 'trying revision: main'
            this.script.checkout(this.checkoutArgs << args << [revision: 'main'])
            post_checkout()
        }
        */
    }
}
