package com.infineon.helpers

class JenkinsHelper implements Serializable
{
    def script

    JenkinsHelper(script)
    // (Object)
    {
        this.script = script
    }

    def getRevId(repo)
    // (GitSCM)
    {
        this.script.node('master')
        {
            def revId = this.script.sh returnStdout: true, script: "git rev-list --count HEAD ${repo.changeset_hash} ${repo.source}"
            return revId.trim()
        }
    }
    
    def getLastCommitUser(repo)
    // (GitSCM)
    {
        this.script.node('master')
        {
            this.script.dir('')
            {
                def lastCommitUser = this.script.sh returnStdout: true, script: " git log -1 --pretty=format:\'%an\' "
                return lastCommitUser.trim()
            }
        }
    }
}
