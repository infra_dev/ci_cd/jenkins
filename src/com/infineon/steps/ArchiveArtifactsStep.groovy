package com.infineon.steps
import com.infineon.helpers.*
import com.infineon.steps.AbstractStep

class ArchiveArtifactsStep extends AbstractStep
{
    ArchiveArtifactsStep(script)
    // (Object, GitSCM)
    {
        super('Archive Artifacts', script)
    }

    def execute()
    {
        return {
            this.script.node('ml_dev_node-1')
            {
            	try {
                        this.script.archiveArtifacts 'logs/**'                        
                    }
                    catch(Exception e) {
                        this.script.echo e.toString()
                        this.script.error("Pipeline failed while archiving artifacts")
                    }

            }               
        }
    }
}