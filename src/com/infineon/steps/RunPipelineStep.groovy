package com.infineon.steps
import com.infineon.helpers.*
import com.infineon.steps.AbstractStep

class RunPipelineStep extends AbstractStep
{
    private def jenkins_helper
    private def pipeline_name
    //private def env = System.getenv()

    RunPipelineStep(script, pipeline_name)
    // (Object, GitSCM)
    {
        super('Step ' + pipeline_name, script)
        this.pipeline_name = pipeline_name
    }

    def execute()
    {
        return {
            this.script.node('master')
            {
                try {

                    this.script.build job: 'Audio_ML_Demo'
                }
                catch(Exception e) {
                    this.script.echo e.toString()
                    this.script.error("Pipeline failed at Step 1: Dataset Creation")
                }
            }
            this.jenkins_helper = null 
        }
    }
}
