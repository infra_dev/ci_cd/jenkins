package com.infineon.steps

interface StepInterface extends Serializable
{
    def execute()
}
