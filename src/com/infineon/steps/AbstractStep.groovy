package com.infineon.steps

abstract class AbstractStep implements StepInterface
{
    protected def name
    protected def script

    AbstractStep(name, script)
    // (String, Object)
    {
        this.name = name
        this.script = script
    }

    public def getName()
    { name }

    abstract def execute()
}
