package com.infineon.steps
import com.infineon.helpers.*
import com.infineon.steps.AbstractStep

class RepoUpdateStep extends AbstractStep
{

    private def repo_name

    RepoUpdateStep(script, repo_name)
    // (Object, GitSCM)
    {
        super('Repo Update ' + repo_name, script)
        this.repo_name = repo_name
    }

    def execute()
    {
        return {
            this.script.node('ml_dev_node-1')
            {
            	try {

                    if(repo_name == 'jenkins'){
                        this.script.sh "if cd jenkins; then git checkout main; git pull --verbose; git log -n 1; git log --graph > ../git_graph.log; else git clone https://gitlab.com/infra_dev/ci_cd/jenkins.git; fi"
                    }
                }
                catch(Exception e) {
                    this.script.echo e.toString()
                    this.script.error("Pipeline failed while updating Repository")
                }
            }               
        }
    }
}