package com.infineon.steps
import com.infineon.helpers.*
import com.infineon.steps.AbstractStep

class RunScriptStep extends AbstractStep
{
    private def step_id
    private def step_name
    private def jenkins_helper
    private def py_path
    private def script_path
    private def command
    private def ret_val
    //private def env = System.getenv()

    // RunScriptStep(script, step_id, step_name, py_path, script_path)

    RunScriptStep(script, step_id, step_name)
    // (Object, GitSCM)
    {
        super('Step ' + step_name, script)
        this.step_name = step_name
        this.step_id = step_id
    }

    def execute()
    {
        return {
            this.script.node('ml_dev_node-1')
            {
                // this.script.sh "/home/audio_ml/virtualenvs/audio_ml_venv/bin/python --version"

                if (step_id == '1')
                {

                    try {

                        this.script.sh "mkdir -p logs/DATASET_CREATION"
                        this.script.sh "cp -r /home/audio_ml/group_space/chungathzash/ml-infra-dev-repo/JENKINS_EXPERIMENTS/DATA_FETCHING/logs/* logs/DATASET_CREATION/"
                        
                        ret_val = this.script.sh "python3 jenkins/pythonScripts/verifylog.py --folder_path \"/home/audio_ml/group_space/chungathzash/ml-infra-dev-repo/JENKINS_EXPERIMENTS/DATA_FETCHING/logs\""
                        if(ret_val == 1)
                        {
                            this.script.echo "verifylog.py returned 1"
                        }
                        
                    }
                    catch(Exception e) {
                        this.script.archiveArtifacts 'logs/**'
                        this.script.echo e.toString()
                        this.script.error("Pipeline failed at Step 1: Dataset Creation")
                    }

                    // this.script.withPythonEnv("/home/audio_ml/virtualenvs/audio_ml_venv/bin/python") {
                    //     this.script.sh "python --version"
                    // }
                }
                else if (step_id == '2')
                {

                    try {
                        
                        this.script.sh "mkdir -p logs/PHONEME_GENRATION"
                        this.script.sh "cp -r /home/audio_ml/group_space/chungathzash/ml-infra-dev-repo/JENKINS_EXPERIMENTS/PHONEME_GENRATION/logs/* logs/PHONEME_GENRATION/"
                        
                        ret_val = this.script.sh "python3 jenkins/pythonScripts/verifylog.py --folder_path \"/home/audio_ml/group_space/chungathzash/ml-infra-dev-repo/JENKINS_EXPERIMENTS/PHONEME_GENRATION/logs\""
                        if(ret_val == 1)
                        {
                            this.script.echo "verifylog.py returned 1"
                        }
                        
                    }
                    catch(Exception e) {
                        this.script.archiveArtifacts 'logs/**'
                        this.script.echo e.toString()
                        this.script.error("Pipeline failed at Step 2: Phoneme Generation")
                    }
                }
                else
                {
                    this.script.echo ""
                }
            }
            this.jenkins_helper = null 
        }
    }
}
