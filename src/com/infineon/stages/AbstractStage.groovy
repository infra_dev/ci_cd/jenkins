package com.infineon.stages

abstract class AbstractStage implements StageInterface
{
    protected def name
    protected def steps

    AbstractStage(name)
    {
        this.name = name
        this.steps = [:]
    }

    def addStep(step_)
    // (Step)
    {
        this.steps[step_.getName()] = step_.execute()
        this
    }

    abstract void execute()
}
