package com.infineon.stages

import com.infineon.stages.AbstractStage

class Stage extends AbstractStage
{
    protected def script
    protected def parallel

    Stage(name, script, parallel=false)
    // (String, Object, Boolean)
    {
        super(name)
        this.script = script
        this.parallel = parallel
    }

    @Override
    void execute()
    {
        if (this.parallel)
        {
            this.script.stage(this.name)
            {
                this.script.parallel this.steps
            }
        }
        else
        {
            def keys = this.steps.keySet() as List
            for(int i = 0; i < keys.size(); ++i)
            {
                // using this.steps.each{} was only running the first element
                // which might be an error with jenkins
                this.script.stage(keys.get(i))
                {
                    this.steps[keys.get(i)]()
                }
            }
        }
    }
}
