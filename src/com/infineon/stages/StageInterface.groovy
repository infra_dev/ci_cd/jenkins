package com.infineon.stages

interface StageInterface extends Serializable
{
    void execute()
}

