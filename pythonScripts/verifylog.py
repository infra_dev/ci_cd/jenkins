import os.path
import argparse
import sys
import re
import glob

def main():

    parser = argparse.ArgumentParser(description='Pass your absolute file path')
    parser.add_argument('--folder_path', default=None, type=str,
                        help='Log folder path, /home/audio_ml/group_space/chungathzash/ml-infra-dev-repo/JENKINS_EXPERIMENTS/DATA_FETCHING/logs')

    args = parser.parse_args(sys.argv[1:])

    folder_path = args.folder_path

    list_of_files = glob.glob(folder_path + "/*") # * means all if need specific format then *.log
    latest_file = max(list_of_files, key=os.path.getctime)
    print("Latest log file = " + latest_file)

    pattern_success = r'(STATUS)(: )(SUCCESS)'
    pattern_fail = r'(STATUS)(: )(FAILED)'
    pattern_stageID = r'(STAGE_ID:)'
    with open(latest_file) as f:
        lines = [line.rstrip() for line in f]

    for l in lines:
        for match in re.finditer(pattern_stageID,l):
            print(l)

    for l in lines:
        for match in re.finditer(pattern_success,l):
            print(l)
            return(0)

        for match in re.finditer(pattern_fail,l):
            print(l)
            sys.exit(1)

if __name__ == "__main__":
    main()